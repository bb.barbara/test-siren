<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Validator\Constraints\SirenProperties;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Company
 * @ApiResource(
 *     itemOperations=Company::ITEM_OPERATIONS,
 *     collectionOperations=Company::COLLECTION_OPERATIONS
 * )
 * @ApiFilter(SearchFilter::class, properties={"siren": "exact"})
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /*
     * itemOperations
     */
    const ITEM_OPERATIONS = [
        "get" => [],
        "put" => [],
        "delete" => []
    ];

    /**
     * collectionOperations
     */
    const COLLECTION_OPERATIONS = [
        "get" => [],
        "post" => [],
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Numéro SIREN
     * @ORM\Column(type="string", length=9)
     * @SirenProperties()
     * @Assert\Length(
     *      min = 9,
     *      max = 9,
     *      minMessage = "Le numéro de SIREN doit être de 9 caractères",
     *      maxMessage = "Le numéro de SIREN doit être de 9 caractères"
     * )
     */
    private $siren;

    /**
     * Nom de la société
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(
     *      min = 5,
     *      max = 100,
     *      minMessage = "Le nom de la société doit être de minimum 5 caractères",
     *      maxMessage = "Le nom de la société doit être de minimum 100 caractères"
     * )
     */
    private $companyName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren(string $siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }
}
