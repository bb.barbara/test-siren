<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * SirenProperties
 * Validation d'un numéro de SIREN
 * @Annotation
 */
class SirenProperties extends Constraint
{
    public $message = 'Veuillez saisir un numéro de siren valide.';

    /**
     * Valide un numéro de SIREN
     * @param $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        /** @TODO mettre les conditions pour valider un SIREN */
        // $this->context->buildViolation($constraint->message)->addViolation();
    }
}