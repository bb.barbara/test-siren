<?php

namespace App\Controller;

use App\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Container\ContainerInterface;

/**
 * Class CompanyController
 * @package App\Controller
 */
class CompanyController extends AbstractController
{
    /**
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;

    /**
     * CompanyController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->doctrine = $container->get('doctrine');
    }

    /**
     * Importation des sociétés depuis le dernier CSV
     *
     * @Route("/cron/import_companies", name="import_companies")
     */
    public function index()
    {
        /**
         * @TODO télécharger le dernier fichier sur le site
         * https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/
         * l'extraire pour l'importation des données
         */
        $pathFile =  __DIR__ . "/../../public/import/sirc-17804_9075_14211_2018088_E_Q_20180330_020005457.csv";

        $repository = $this->getDoctrine()->getRepository(Company::class);

        // Liste de toutes les sociétés
        $companies = $repository->findAll();

        $imported = [];
        $error = null;
        try {
            if (($handle = fopen($pathFile, "r")) !== FALSE) {
                $row = 1;
                while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
                    if($row > 1) {

                        /* vérifier que la société n'existe pas déjà */
                        $company = new Company();
                        $company->setSiren($data[0]);
                        $company->setCompanyName($data[2]);
                        $finds = array_filter(
                            $companies,
                            function ($e) use (&$company) {
                                if($e instanceof Company) {
                                    return ($company->getSiren() === $e->getSiren());
                                }
                            }
                        );

                        // Si aucun résultat, on importe
                        if(count($finds) == 0) {
                            $this->doctrine->getManager()->persist($company);
                            $imported[] = $company;
                        }
                    }
                    $row++;
                }
                fclose($handle);

                $this->doctrine->getManager()->flush();
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        return $this->render('company/import_companies.html.twig', [
            'companies' => $imported,
            'error' => $error
        ]);
    }
}
